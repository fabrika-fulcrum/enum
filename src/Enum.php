<?php
namespace Fulcrum\Enum;

use ReflectionClass;

abstract class Enum
{
    protected $value = '';

    final public function __construct($value)
    {
        $c = new ReflectionClass($this);
        if (!in_array($value, $c->getConstants())) {
            throw new IllegalArgumentException();
        }
        $this->value = $value;
    }

    final public function __toString()
    {
        return $this->value;
    }

    final public function value(){
        return $this->value;
    }

}
